﻿using System;
using System.Collections.Generic;

namespace iolab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");           
        }
    }

    public class Calculator
    {
        public int calculate(string value)
        {
            if (value == "")
                return 0;

            if (value.Contains("-"))
            {
                throw new NegativeNumberException();
            }

            if (value.StartsWith("//"))
            {
                if (value[2] == '[')
                {
                    List<string> signs = new List<string>();
                    
                    var sign = value.Substring(3, 2);
                    var splited = value.Substring(6).Split(sign);
                    var sub = value.Substring(6);
                    return sum(splited);
                }
                else
                {
                    var splited = value.Substring(2).Split(value[2]);
                    var sub = value.Substring(2);
                    return sum(splited);
                }                
            }

            if (int.TryParse(value, out int n))
            {
                return n>=1000?0:n;
            }

            if (value.Contains(","))
            {
                var splited = value.Split(",");
                return sum(splited);
            }

            if (value.Contains("\n"))
            {
                var splited = value.Split("\n");
                return sum(splited);
            }

            return 0;
        }

        private static int sum(string[] splited)
        {
            var result = 0;
            foreach (var item in splited)
            {
                if (item == "")
                    continue;
                int number = int.Parse(item);
                if (number >= 1000)
                    number = 0;
                result += number;
            }
            return result;
        }
    }

    public class NegativeNumberException : Exception
    {

    }
}