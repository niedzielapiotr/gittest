using Microsoft.VisualStudio.TestTools.UnitTesting;
using iolab1;

namespace Test
{
    [TestClass]
    public class CalculatorTest
    {
        private Calculator calculator;
        public CalculatorTest()
        {
            calculator = new Calculator();
        }

        [TestMethod]
        public void testForEmptyString()
        {
            // Given
            string given = "";
            //When
            int result = calculator.calculate(given);
            //Then
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void testForNumbers1()
        {
            // Given
            string given = "1";
            //When
            int result = calculator.calculate(given);
            //Then
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void testForNumbers2()
        {
            // Given
            string given = "8";
            //When
            int result = calculator.calculate(given);
            //Then
            Assert.AreEqual(result, 8);
        }

        [TestMethod]
        public void testForSum()
        {
            // Given
            string given = "2,4";
            //When
            int result = calculator.calculate(given);
            //Then
            Assert.AreEqual(result, 6);
        }

        [TestMethod]
        public void testForSlashN()
        {
            // Given
            string enter = "2\n4";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 6);
        }

        [TestMethod]
        public void testForSumOfMany()
        {
            // Given
            string enter = "3,4,6";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 13);
        }

        [TestMethod]
        public void testForSumOfMany2()
        {
            // Given
            string enter = "3\n4\n6";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 13);
        }

        [TestMethod]
        public void testForNegativeNumbers()
        {
            // Given
            string enter = "-1";
            //When
            
            //Then
            Assert.ThrowsException<NegativeNumberException>(()=>calculator.calculate(enter));
        }

        [TestMethod]
        public void testForNegativeNumbers2()
        {
            // Given
            string enter = "3,-2,15";
            
            //Then
            Assert.ThrowsException<NegativeNumberException>(() => calculator.calculate(enter));
        }

        [TestMethod]
        public void testForNumbersOver1000()
        {
            // Given
            string enter = "15,2000,7";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 22);
        }

        [TestMethod]
        public void testForNumbersOver10002()
        {
            // Given
            string enter = "1054";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void testForSpecialSign()
        {
            // Given
            string enter = "//#4#5#7";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 16);
        }

        [TestMethod]
        public void testForSpecialSign2()
        {
            // Given
            string enter = "//#4#5";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 9);
        }

        [TestMethod]
        public void testForDoubleLimitter()
        {
            // Given
            string enter = "//[??]7??2";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 9);
        }

        public void testForTwoLimitters()
        {
            // Given
            string enter = "//[??][#]4#12??3";
            //When
            int result = calculator.calculate(enter);
            //Then
            Assert.AreEqual(result, 22);
        }
    }
}